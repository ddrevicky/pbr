Physically Based Rendering
Dusan Drevicky
Term Project for PGPa 2017/2018

project structure:
pbr/
  -----build
  -----include
  -----lib
  -----lic
  -----resources
  -----src
  CMakelists.tx
  README.txt

To run the project:

1. The include/SDL2/sdl_configs directory ontains two subdirectories: /unix and /win32. Copy the SDL_config.h file
   from the directory corresponding to the system you are running to the include/SDL2/ overwriting if necessary. (This file
   is the result of SDL build process and thus dependent on the platform it was built for).
2. Run cmake on the root pbr/ directory and place the build files in the /build directory.
3. If running on Windows: Place /lib/win32/SDL2.dll to the directory containing the executable.